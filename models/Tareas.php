<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tareas".
 *
 * @property int $id_tarea
 * @property string $nombre
 * @property string $asunto
 * @property string $descripcion
 * @property int $propietario
 * @property string $fecha_inicio
 * @property string $fecha_fin
 *
 * @property Usuarios $propietario0
 */
class Tareas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tareas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'asunto', 'descripcion', 'propietario'], 'required'],
            [['descripcion'], 'string'],
            [['propietario'], 'integer'],
            [['fecha_inicio', 'fecha_fin'], 'safe'],
            [['nombre', 'asunto'], 'string', 'max' => 255],
            [['propietario'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['propietario' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tarea' => 'Id Tarea',
            'nombre' => 'Nombre',
            'asunto' => 'Asunto',
            'descripcion' => 'Descripcion',
            'propietario' => 'Propietario',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_fin' => 'Fecha Fin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropietario0()
    {
        return $this->hasOne(User::className(), ['id' => 'propietario']);
    }

    /**
     * @inheritdoc
     * @return TareasQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TareasQuery(get_called_class());
    }
}
