<?php
/* @var $this yii\web\View */
use yii\helpers\Url;

?>
<div class="site-index">

    <div class="jumbotron">
        <img src=<?= Url::to("@web/imgs/logo.png")?> alt="Gestor de tareas">   
         <div class="row">
            <div class="col-lg-8 right">
                    <p><a class="btn btn-lg btn-warning" href=<?= Url::to(["/site/registrar"])?>>Registrate &raquo;</a></p>
            </div>
            <div class="col-lg-1 left">
                <p><a class="btn btn-lg btn-warning" href= <?= Url::to(["/site/login"])?>>Logueate &raquo;</a></p>
            </div>     
        </div>
        <p class="lead">El registro es gratuito, no te llevará más de dos minutos.</p>
    </div>
    </div>
 
        
    

    <div class="body-content">
        <div class="jumbotron">
            <div class="row">
                <div class="col-lg-6">
                    <h2>¿Qué es Gestor de tareas?</h2>

                    <p>Un gestor de tareas que te ayudará a conseguir los objetivos que te marque s de la forma más facíl posible.
                    Nunca se te olvidará ninguna cita.
                    No pierdas más el tiempo ni las oportunidades que te esperan.</p>
                
     

                    <p><a class="btn btn-default" href="/ayuda">Más información &raquo;</a></p>
                </div>
                <div class="col-lg-6">
                    <h2>¿Qué ofrece Gestor de tareas?</h2>
                    <p> Gestor de Treas es gratuito, ofrece un cuadrante activo en el que te será muy facíl administrar tus cosas</p>

                    <p><a class="btn btn-default" href="/ayuda">Más información &raquo;</a></p>
                </div>
            </div>
       </div>
    </div>
</div>
