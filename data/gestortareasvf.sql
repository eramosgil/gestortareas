-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-01-2018 a las 14:07:20
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gestortareasvf`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tareas`
--

CREATE TABLE `tareas` (
  `id_tarea` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `asunto` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` text COLLATE latin1_spanish_ci NOT NULL,
  `propietario` int(11) NOT NULL DEFAULT '13',
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `tareas`
--

INSERT INTO `tareas` (`id_tarea`, `nombre`, `asunto`, `descripcion`, `propietario`, `fecha_inicio`, `fecha_fin`) VALUES
(1, 'tarea1', 'asunto tarea 1', 'descripcion tarea 1', 14, '0000-00-00', '0000-00-00'),
(2, 'ss', 'ss', 'sss', 13, '0000-00-00', '0000-00-00'),
(3, 'tarea3', 'jks', 'wqjwq', 14, '0000-00-00', '0000-00-00'),
(5, 's', 's', 'ss', 14, '2018-01-22', '2018-01-12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `apellidos` varchar(255) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(55) NOT NULL,
  `authKey` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '0',
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=16384 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `nombre`, `apellidos`, `email`, `username`, `authKey`, `activo`, `password`) VALUES
(13, 'admin', NULL, 'admin@admin.es', 'admin', 'QwgSGtruVgy-RCGyQ2UnLtC8b8uZ5q3Q', 1, '$2y$13$6ZJcWniodgcL7Kerok975.zPJ7HotRpV0fjVpNI4FOVk26vFPwHiq'),
(14, 'emma', NULL, 'eramosgil@gmail.com', 'emma', 'Y_gavXEL1OEPI0pOW-1u1ORdzajSyfGa', 1, '$2y$13$mWqHkuqz0VZOeK/oouTBgOSJ8Gd.TCUI9FwzlvbtqhWVe881r6RUK');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tareas`
--
ALTER TABLE `tareas`
  ADD PRIMARY KEY (`id_tarea`),
  ADD KEY `FK_Tareas_usuarios_id` (`propietario`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_usuarios_email` (`email`),
  ADD UNIQUE KEY `UK_usuarios_username` (`username`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tareas`
--
ALTER TABLE `tareas`
  MODIFY `id_tarea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
